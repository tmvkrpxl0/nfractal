use std::fs::File;
use std::io::Write;
use std::path::Path;
use itertools::Itertools;
use png::Decoder;

fn main() {
    let path = Path::new("img.png");
    let decoder = Decoder::new(File::open(path).unwrap());
    let mut reader = decoder.read_info().unwrap();
    let mut entire_image = vec![0; reader.output_buffer_size()];
    let info = reader.next_frame(&mut entire_image).unwrap();

    let rgb = &entire_image[..info.buffer_size()];
    let channels = rgb.len() / (1920 * 1080);
    let chunked = rgb.chunks_exact(channels);
    if !chunked.remainder().is_empty() {
        panic!("File size is... wrong? expected channel: {}, actual size: {}, remaining bytes: {}", channels, rgb.len(), chunked.remainder().len());
    }
    let as_byte = chunked.map(|value| {
        let average = (value[0] as u16 + value[1] as u16 + value[2] as u16) / 3;
        average as u8
    }).collect_vec();

    let new_file_name = String::from("img.bin");
    let mut new_file = File::options()
        .write(true)
        .create(true)
        .open(new_file_name.trim())
        .unwrap();

    new_file.write_all(as_byte.as_slice()).unwrap();
}