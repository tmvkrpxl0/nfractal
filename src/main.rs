use pollster::block_on;
use nfractal::run;

fn main() {
    block_on(run())
}
