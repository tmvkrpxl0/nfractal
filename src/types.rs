use bytemuck::{Pod, Zeroable};
use std::mem::size_of;
use wgpu::VertexFormat::Float32x3;
use wgpu::{VertexAttribute, VertexBufferLayout};

#[repr(C)]
#[derive(Copy, Clone, Debug, Pod, Zeroable)]
pub struct Vertex {
    pub position: [f32; 3],
}

impl Vertex {
    pub fn create_layout<'a>() -> VertexBufferLayout<'a> {
        VertexBufferLayout {
            array_stride: size_of::<Vertex>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &[VertexAttribute {
                format: Float32x3,
                offset: 0,
                shader_location: 0,
            }],
        }
    }
}
