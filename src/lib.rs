#![feature(new_uninit)]

use std::iter::once;
use std::mem::size_of;
use std::sync::Arc;

use bytemuck::cast_slice;
use cfg_if::cfg_if;
use eframe::egui;
use eframe::egui::{Context, FontData, FontDefinitions, FontFamily, Slider};
use egui_wgpu::{Renderer, ScreenDescriptor};
#[cfg(target_arch = "wasm32")]
use log::debug;
use toodee::TooDeeOps;
use wgpu::*;
use wgpu::Face::Back;
use wgpu::FrontFace::Ccw;
use wgpu::IndexFormat::Uint16;
use wgpu::PolygonMode::Fill;
use wgpu::PrimitiveTopology::TriangleList;
use wgpu::ShaderSource::Wgsl;
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use winit::{
    event::*,
    event_loop::EventLoop,
};
use winit::dpi::PhysicalSize;
use winit::keyboard::{KeyCode, PhysicalKey};
use winit::window::{Window, WindowBuilder};

#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;

use crate::simulation::*;
use crate::simulation::NeuronState::Active;
use crate::types::*;

mod simulation;
mod types;

const TEXTURE_SIZE: Extent3d = Extent3d {
    width: 1920,
    height: 1080,
    depth_or_array_layers: 1,
};

struct State {
    surface: Surface<'static>,
    device: Device,
    screen: ScreenDescriptor,
    queue: Queue,
    config: SurfaceConfiguration,
    size: PhysicalSize<u32>,
    window: Arc<Window>,
    render_pipeline: RenderPipeline,
    vertex_buffer: Buffer,
    index_buffer: Buffer,
    simulation: Simulation,
    world_bind_group: BindGroup,
    world_texture: Texture,
    egui_state: egui_winit::State,
    egui_context: Context,
    egui_renderer: Renderer,
    show_menu: bool,
    frame_count: usize,
    tick_once_per: u32,
    mouse_influence_size: f32,
    is_dragging: bool,
    mouse_x: usize,
    mouse_y: usize,
}

impl State {
    // Creating some of the wgpu types requires async code
    async fn new(window: Window, event_loop: &EventLoop<()>) -> Self {
        log::info!("Constructing State..");

        let window: Arc<_> = window.into();
        let size = window.inner_size();

        let instance = Instance::new(InstanceDescriptor::default());

        let surface = instance.create_surface(window.clone()).unwrap();
        let adapter = instance
            .request_adapter(&RequestAdapterOptions {
                power_preference: Default::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .unwrap();

        let device_descriptor = DeviceDescriptor {
            label: None,
            required_features: Default::default(),
            required_limits: {
                let mut limit = Limits::downlevel_webgl2_defaults();
                limit.max_uniform_buffer_binding_size = 1920 * 1080;
                limit
            },
        };
        let (device, queue) = adapter
            .request_device(&device_descriptor, None)
            .await
            .unwrap();

        let surface_caps = surface.get_capabilities(&adapter);
        // Shader code in this tutorial assumes an sRGB surface texture. Using a different
        // one will result all the colors coming out darker. If you want to support non
        // sRGB surfaces, you'll need to account for that when drawing to the frame.
        let surface_format = surface_caps
            .formats
            .iter().find(|f| f.is_srgb())
            .copied()
            .unwrap_or(surface_caps.formats[0]);
        let config = SurfaceConfiguration {
            usage: TextureUsages::RENDER_ATTACHMENT,
            format: surface_format,
            width: size.width,
            height: size.height,
            present_mode: surface_caps.present_modes[0],
            desired_maximum_frame_latency: 2,
            alpha_mode: surface_caps.alpha_modes[0],
            view_formats: vec![],
        };
        surface.configure(&device, &config);

        let shader = device.create_shader_module(ShaderModuleDescriptor {
            label: Some("nfractal shader"),
            source: Wgsl(include_str!("shader.wgsl").into()),
        });

        let world_texture = device.create_texture(
            &TextureDescriptor {
                label: Some("world_texture"),
                size: TEXTURE_SIZE,
                mip_level_count: 1,
                sample_count: 1,
                dimension: TextureDimension::D2,
                format: TextureFormat::R8Unorm,
                usage: TextureUsages::TEXTURE_BINDING | TextureUsages::COPY_DST,
                view_formats: &[],
            }
        );
        queue.write_texture(
            ImageCopyTexture {
                texture: &world_texture,
                mip_level: 0,
                origin: Default::default(),
                aspect: Default::default(),
            },
            vec![0u8; 1920 * 1080].as_slice(),
            ImageDataLayout {
                offset: 0,
                bytes_per_row: 1920.into(),
                rows_per_image: 1080.into(),
            },
            TEXTURE_SIZE,
        );
        let world_view = world_texture.create_view(&TextureViewDescriptor::default());
        let world_sampler = device.create_sampler(&SamplerDescriptor {
            label: Some("world samper"),
            ..Default::default()
        });
        let world_bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: Some("world bind group layout"),
            entries: &[
                BindGroupLayoutEntry {
                    binding: 0,
                    visibility: ShaderStages::FRAGMENT,
                    ty: BindingType::Texture {
                        sample_type: Default::default(),
                        view_dimension: Default::default(),
                        multisampled: false,
                    },
                    count: None,
                },
                BindGroupLayoutEntry {
                    binding: 1,
                    visibility: ShaderStages::FRAGMENT,
                    ty: BindingType::Sampler(SamplerBindingType::Filtering),
                    count: None,
                }
            ],
        });
        let world_bind_group = device.create_bind_group(&BindGroupDescriptor {
            label: Some("world bind group"),
            layout: &world_bind_group_layout,
            entries: &[
                BindGroupEntry {
                    binding: 0,
                    resource: BindingResource::TextureView(&world_view),
                },
                BindGroupEntry {
                    binding: 1,
                    resource: BindingResource::Sampler(&world_sampler),
                }
            ],
        });

        let pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: Some("nfractal pipeline layout"),
            bind_group_layouts: &[&world_bind_group_layout],
            push_constant_ranges: &[],
        });
        let render_pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
            label: Some("nfractal pipeline"),
            layout: Some(&pipeline_layout),
            vertex: VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[Vertex::create_layout()], // Here, Array of Vertex Buffer Layout is passed
                // Which is needed later in set_vertex_buffer
            },
            fragment: Some(FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[Some(ColorTargetState {
                    format: config.format,
                    blend: Some(BlendState::REPLACE),
                    write_mask: ColorWrites::ALL,
                })],
            }),
            primitive: PrimitiveState {
                topology: TriangleList,
                strip_index_format: None,
                front_face: Ccw,
                cull_mode: Some(Back),
                unclipped_depth: false,
                polygon_mode: Fill,
                conservative: false,
            },
            depth_stencil: None,
            multisample: MultisampleState {
                count: 1,
                mask: 0xFFFFFFFFFFFFFFFF,
                alpha_to_coverage_enabled: false,
            },
            multiview: None,
        });

        cfg_if! {
            if #[cfg(target_arch = "wasm32")] {
                debug!("Before setting up Vertex Buffer");
            }
        }

        let vertex_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Vertex buffer"),
            contents: cast_slice(VERTICES),
            usage: BufferUsages::VERTEX,
        });
        let index_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Index buffer"),
            contents: cast_slice(INDICES),
            usage: BufferUsages::INDEX,
        });

        let simulation = Simulation::new(1, 1.0, 1.0, 3.0, 2);
        let egui_context = Context::default();
        let egui_state = egui_winit::State::new(
            egui_context.clone(),
            egui_context.viewport_id(),
            event_loop,
            egui_context.native_pixels_per_point(),
            None,
        );

        let fonts = {
            let mut f = FontDefinitions {
                font_data: Default::default(),
                families: Default::default(),
            };

            f.font_data.insert(
                "Nanum Gothic".to_owned(),
                FontData::from_static(include_bytes!("../NanumGothic.ttf")),
            );
            f.font_data.insert(
                "Nanum Gothic Coding".to_owned(),
                FontData::from_static(include_bytes!("../NanumGothicCoding.ttf")),
            );

            f.families
                .insert(FontFamily::Proportional, vec!["Nanum Gothic".to_owned()]);
            f.families.insert(
                FontFamily::Monospace,
                vec!["Nanum Gothic Coding".to_owned()],
            );

            f
        };
        egui_context.set_fonts(fonts);

        let egui_renderer = Renderer::new(&device, surface_format, None, 1);
        let screen = ScreenDescriptor {
            size_in_pixels: [config.width, config.height],
            pixels_per_point: egui_context.pixels_per_point(),
        };

        Self {
            surface,
            device,
            screen,
            queue,
            config,
            size,
            window,
            render_pipeline,
            vertex_buffer,
            index_buffer,
            simulation,
            world_bind_group,
            world_texture,
            egui_state,
            egui_context,
            egui_renderer,
            show_menu: false,
            frame_count: 0,
            tick_once_per: 6,
            mouse_influence_size: 5.0,
            is_dragging: false,
            mouse_x: 0,
            mouse_y: 0,
        }
    }

    pub fn window(&self) -> &Window {
        &self.window
    }

    fn resize(&mut self, new_size: PhysicalSize<u32>) {
        if new_size.width == 0 || new_size.height == 0 {
            return;
        }

        self.size = new_size;
        self.config.width = new_size.width;
        self.config.height = new_size.height;
        self.surface.configure(&self.device, &self.config);

        self.screen.pixels_per_point = self.egui_context.pixels_per_point();
        self.screen.size_in_pixels = [self.config.width, self.config.height];
    }

    fn tick_simulation(&mut self) {
        self.simulation.tick()
    }

    fn render(&mut self) -> Result<(), SurfaceError> {
        if self.is_dragging {
            let x = self.mouse_x * self.simulation.scale;
            let y = self.mouse_y * self.simulation.scale;
            let boundary = (0..self.simulation.map.num_cols(), 0..self.simulation.map.num_rows());

            traverse(
                x,
                y,
                self.mouse_influence_size,
                boundary,
                |c| {
                    self.simulation.activate_one(c.0, c.1);
                },
            );
        }

        let frame = self.surface.get_current_texture()?;
        let view = frame.texture.create_view(&Default::default());

        let mut encoder = self
            .device
            .create_command_encoder(&CommandEncoderDescriptor {
                label: Some("nfractal encoder"),
            });

        let output = self
            .egui_context
            .run(self.egui_state.take_egui_input(&self.window), |ctx| {
                egui::Window::new("설정")
                    .open(&mut self.show_menu)
                    .show(ctx, |ui| {
                        ui.label(format!("포화도: {}", {
                            self.simulation.map
                                .cells()
                                .filter(|v| v.state == Active)
                                .count() as f32 / self.simulation.map.cells().len() as f32
                        }));
                        ui.label(format!("규모: {}", self.simulation.scale));
                        if self.simulation.loaded_image.is_none() {
                            if ui.button("규모+").clicked() && self.simulation.scale < 6 {
                                self.simulation = Simulation::new(
                                    self.simulation.scale + 1,
                                    self.simulation.max_distance,
                                    self.simulation.signal_strength,
                                    self.simulation.threshold,
                                    self.simulation.refractory_period,
                                )
                            }
                            if ui.button("규모-").clicked() && self.simulation.scale > 1 {
                                self.simulation = Simulation::new(
                                    self.simulation.scale - 1,
                                    self.simulation.max_distance,
                                    self.simulation.threshold,
                                    self.simulation.threshold,
                                    self.simulation.refractory_period,
                                )
                            }
                        }
                        if ui.button("tick").clicked() {
                            self.simulation.tick()
                        }
                        ui.label("브러시 크기");
                        ui.add(Slider::new(
                            &mut self.mouse_influence_size,
                            0.0f32..=400.0f32,
                        ));
                        ui.label("시뮬레이션 대기 시간");
                        ui.add(Slider::new(
                            &mut self.tick_once_per,
                            0..=10u32,
                        ));
                        if self.tick_once_per == 0 {
                            ui.label("정지됨");
                        };
                        ui.label("신호 세기");
                        ui.add(Slider::new(
                            &mut self.simulation.signal_strength,
                            0.0..=500.0,
                        ));
                        ui.label("역치 전위");
                        ui.add(Slider::new(
                            &mut self.simulation.threshold,
                            0.0..=500.0,
                        ));
                        ui.label("휴지 전위 유지 시간");
                        ui.add(Slider::new(
                            &mut self.simulation.refractory_period,
                            1..=120i8,
                        ));
                        if ui.button("신경 위치 뒤섞기").clicked() {
                            self.simulation.set_shuffle(!self.simulation.is_shuffled())
                        }
                        ui.label("신경 상호작용 거리");
                        ui.add(Slider::new(
                            &mut self.simulation.max_distance,
                            0.0..=100.0,
                        ));
                        let is_loaded = self.simulation.loaded_image.is_some();
                        if ui.selectable_label(is_loaded, "이미지 모드").clicked() {
                            if is_loaded {
                                self.simulation.loaded_image = None;
                            } else {
                                if self.simulation.scale != 1 {
                                    self.simulation = Simulation::new(
                                        1,
                                        self.simulation.max_distance,
                                        self.simulation.threshold,
                                        self.simulation.threshold,
                                        self.simulation.refractory_period,
                                    )
                                };
                                self.simulation.load_file();
                            }
                        };
                        ui.checkbox(&mut self.simulation.random_activation, "확률적 활성화");
                        if ui.button("초기화").clicked() {
                            self.simulation.clear()
                        }
                    });
            });
        self.egui_state.handle_platform_output(
            &self.window,
            output.platform_output,
        );

        let primitives = self.egui_context.tessellate(output.shapes, self.egui_context.pixels_per_point());
        for (texture_id, image_delta) in output.textures_delta.set {
            self.egui_renderer
                .update_texture(&self.device, &self.queue, texture_id, &image_delta);
        }
        self.egui_renderer.update_buffers(
            &self.device,
            &self.queue,
            &mut encoder,
            primitives.as_slice(),
            &self.screen,
        );

        {
            let mut render_pass = encoder.begin_render_pass(&RenderPassDescriptor {
                label: Some("nfractal main render pass"),
                color_attachments: &[Some(RenderPassColorAttachment {
                    view: &view,
                    // The resolve_target is the texture that will receive the resolved output.
                    // This will be the same as view unless multisampling is enabled.
                    // We don't need to specify this, so we leave it as None.
                    resolve_target: None,
                    // Operation is a pair of:
                    // Load(for specifying what to do with previous colors
                    // Store(for specifying whether or not to save result of Load operation into the view)
                    ops: Operations {
                        load: LoadOp::Clear(Color {
                            r: 0.0,
                            g: 0.0,
                            b: 0.0,
                            a: 1.0,
                        }),
                        store: StoreOp::Store,
                    },
                })],
                depth_stencil_attachment: None,
                timestamp_writes: None,
                occlusion_query_set: None,
            });
            render_pass.set_pipeline(&self.render_pipeline);
            render_pass.set_bind_group(0, &self.world_bind_group, &[]);
            // Slot number is index of vertex buffer layout to use
            render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
            render_pass.set_index_buffer(self.index_buffer.slice(..), Uint16);

            let index_count: u32 = {
                let mut size = self.index_buffer.size();
                size = size - (size % 3);
                (size / size_of::<u16>() as u64) as u32
            };
            // base_vertex equals to vertex buffer layout index that we passed before
            render_pass.draw_indexed(0..index_count, 0, 0..1);

            self.egui_renderer
                .render(&mut render_pass, primitives.as_slice(), &self.screen)
        }
        self.queue.write_texture(
            ImageCopyTexture {
                texture: &self.world_texture,
                mip_level: 0,
                origin: Default::default(),
                aspect: Default::default(),
            },
            self.simulation.aggregated.as_slice(),
            ImageDataLayout {
                offset: 0,
                bytes_per_row: 1920.into(),
                rows_per_image: 1080.into(),
            },
            TEXTURE_SIZE,
        );
        self.queue.submit(once(encoder.finish()));

        frame.present();
        if self.tick_once_per != 0 && self.frame_count % self.tick_once_per as usize == 0 {
            self.tick_simulation();
        }
        self.frame_count += 1;

        Ok(())
    }
}

#[cfg_attr(target_arch = "wasm32", wasm_bindgen(start))]
pub async fn run() {
    cfg_if! {
        if #[cfg(target_arch = "wasm32")] {
            std::panic::set_hook(Box::new(console_error_panic_hook::hook));
            console_log::init_with_level(log::Level::Debug).expect("Couldn't initialize logger");
            web_sys::console::log_1(&"WASM 준비 완료! :D".into());
        } else {
            env_logger::init();
        }
    }

    let event_loop = EventLoop::new().unwrap();
    let window = WindowBuilder::new().build(&event_loop).unwrap();

    #[cfg(target_arch = "wasm32")]
    {
        // Winit prevents sizing with CSS, so we have to set
        // the size manually when on web.
        use winit::dpi::PhysicalSize;
        window.set_inner_size(PhysicalSize::new(1920, 1080));

        use winit::platform::web::WindowExtWebSys;
        web_sys::window()
            .and_then(|win| win.document())
            .and_then(|doc| {
                let dst = doc.get_element_by_id("wasm-example")?;
                let canvas = web_sys::Element::from(window.canvas());
                dst.append_child(&canvas).ok()?;
                Some(())
            })
            .expect("Couldn't append canvas to document body.");
    }

    let mut state = State::new(window, &event_loop).await;

    event_loop.run(move |event, target| {
        match event {
            Event::WindowEvent {
                ref event,
                window_id,
            } if window_id == state.window().id() => {
                let response = state.egui_state.on_window_event(&state.window, event);
                if !response.consumed {
                    match event {
                        WindowEvent::Resized(new_size) => {
                            state.resize(*new_size);
                        }
                        WindowEvent::RedrawRequested => match state.render() {
                            Ok(_) => {}
                            Err(SurfaceError::Lost) => state.resize(state.size),
                            Err(SurfaceError::OutOfMemory) => target.exit(),
                            Err(e) => eprintln!("{:?}", e),
                        }
                        WindowEvent::MouseInput {
                            state: clicked,
                            button,
                            ..
                        } => {
                            let is_press = matches!(clicked, ElementState::Pressed);
                            let is_left = matches!(button, MouseButton::Left);

                            if is_left {
                                state.is_dragging = is_press
                            } else if is_press {
                                state.show_menu = !state.show_menu
                            }
                        }
                        WindowEvent::CursorMoved { position, ..} => {
                            state.mouse_x = position.x as usize;
                            state.mouse_y = position.y as usize;
                        }
                        WindowEvent::CloseRequested
                        | WindowEvent::KeyboardInput {
                            event: KeyEvent {
                                state: ElementState::Pressed,
                                physical_key: PhysicalKey::Code(KeyCode::Escape),
                                ..
                            },
                            ..
                        } => target.exit(),
                        _ => {}
                    }
                }
                if response.repaint {
                    state.window.request_redraw();
                }
            }
            Event::AboutToWait => {
                state.window().request_redraw();
            }
            _ => {}
        }
    }).unwrap();
}

const VERTICES: &[Vertex] = &[
    Vertex {
        position: [1.0, 1.0, 0.0],
    },
    Vertex {
        position: [-1.0, 1.0, 0.0],
    },
    Vertex {
        position: [-1.0, -1.0, 0.0],
    },
    Vertex {
        position: [1.0, -1.0, 0.0],
    },
];

const INDICES: &[u16] = &[0, 1, 2, 0, 2, 3];
