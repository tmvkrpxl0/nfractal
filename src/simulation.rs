extern crate core;

use std::fs::File;
use std::io::Read;
use std::mem::transmute;
use std::ops::Range;
use std::sync::atomic::Ordering::Relaxed;

use atomic_float::AtomicF32;
use itertools::Itertools;
use rand::{Rng, thread_rng};
use rand::prelude::SliceRandom;
use rayon::prelude::*;
use toodee::{TooDee, TooDeeOps, TooDeeOpsMut};

use crate::simulation::NeuronState::*;

#[derive(Clone)]
#[derive(Default)]
pub struct Neuron {
    pub state: NeuronState,
}

#[derive(Clone, PartialEq, Eq)]
#[derive(Default)]
pub enum NeuronState {
    Active,
    #[default]
    Inactive,
    Refractory { time: u8 },
}

pub struct Simulation {
    pub map: TooDee<Neuron>,
    pub active_neurons: Vec<(usize, usize)>,
    pub scale: usize,
    pub max_distance: f32,
    pub signal_strength: f32,
    pub threshold: f32,
    shuffler: Option<TooDee<(usize, usize)>>,
    pub refractory_period: i8,
    pub random_activation: bool,
    pub loaded_image: Option<Box<[u8; 1920 * 1080]>>,
    pub aggregated: Box<[u8; 1920 * 1080]>,
}

impl Simulation {
    fn fill_counter(&self) -> TooDee<AtomicF32> {
        let signals: TooDee<AtomicF32> = {
            let mut backing = Vec::with_capacity(self.map.capacity());
            for _ in 0..backing.capacity() {
                backing.push(AtomicF32::default())
            }
            TooDee::from_vec(self.map.num_cols(), self.map.num_rows(), backing)
        };

        match &self.loaded_image {
            None => {
                let to_add = self.signal_strength;
                self.active_neurons.par_iter().for_each(|(x, y)| unsafe {
                    let neuron = self.map.get_unchecked((*x, *y));
                    if let Active = neuron.state {
                        traverse(
                            *x,
                            *y,
                            self.max_distance,
                            (0..self.map.num_cols(), 0..self.map.num_rows()),
                            |to_visit| {
                                if let Inactive = self.map[to_visit].state {
                                    signals[to_visit].fetch_add(to_add, Relaxed);
                                }
                            },
                        );
                    }
                });
            }
            Some(reference) => {
                let transmuted: &[[u8; 1920]; 1080] = unsafe { transmute(reference.as_ref()) };
                self.active_neurons.par_iter().for_each(|(x, y)| unsafe {
                    let neuron = self.map.get_unchecked((*x, *y));
                    if let Active = neuron.state {
                        traverse(
                            *x,
                            *y,
                            self.max_distance,
                            (0..self.map.num_cols(), 0..self.map.num_rows()),
                            |to_visit| {
                                if let Inactive = self.map[to_visit].state {
                                    let mut value = transmuted[*y][*x] as f32 / 255.0;
                                    value -= 0.5;
                                    value *= 2.0;
                                    value *= self.signal_strength;
                                    signals[to_visit].fetch_add(value, Relaxed);
                                }
                            },
                        );
                    }
                });
            }
        }
        signals
    }

    pub fn load_file(&mut self) {
        let mut file = match File::open("img.bin") {
            Ok(f) => f,
            Err(e) => {
                eprintln!("{e}");
                return;
            }
        };

        let mut slice = unsafe { Box::<[u8; 1920 * 1080]>::new_zeroed().assume_init() };
        match file.read_exact(slice.as_mut()).err() {
            None => {}
            Some(e) => {
                eprintln!("{e}");
                return;
            }
        }
        self.loaded_image = Some(slice);
    }

    pub fn tick(&mut self) {
        let signals = self.fill_counter();
        self.active_neurons.clear();

        unsafe {
            for x in 0..1920 * self.scale {
                for y in 0..1080 * self.scale {
                    let neuron = self.map.get_unchecked_mut((x, y));
                    match &mut neuron.state {
                        Active => {
                            neuron.state = Refractory {
                                time: self.refractory_period as u8,
                            }
                        }
                        Inactive => {
                            let count = signals.get_unchecked((x, y));
                            let signal = count.load(Relaxed);
                            let mut top = self.threshold - signal;
                            if top < 0.0 {
                                top = 0.0
                            };

                            let p = if self.random_activation {
                                let mut random = thread_rng();
                                random.gen_range(0.0..1.0)
                            } else {
                                0.99999
                            };

                            if p < 1.0 - top / self.threshold {
                                neuron.state = Active;
                                self.active_neurons.push((x, y));
                            }
                        }
                        Refractory { time } => {
                            *time -= 1;
                            if *time == 0 {
                                neuron.state = Inactive
                            }
                        }
                    };
                }
            };
        }
        self.rebuild_aggregated();
    }

    pub fn is_shuffled(&self) -> bool {
        self.shuffler.is_some()
    }

    pub fn set_shuffle(&mut self, shuffle: bool) {
        self.shuffler = if shuffle {
            let mut shuffled = self.map
                .rows()
                .enumerate()
                .flat_map(|(y, cells)|
                    cells.iter()
                        .enumerate()
                        .map(move |(x, _)| (x, y)))
                .collect_vec();
            shuffled.shuffle(&mut thread_rng());
            Some(TooDee::from_vec(self.map.num_cols(), self.map.num_rows(), shuffled))
        } else {
            None
        };
    }

    pub fn new(
        scale: usize,
        max_distance: f32,
        signal_strength: f32,
        threshold: f32,
        refractory_period: i8,
    ) -> Self {
        let map = TooDee::new(1920 * scale, 1080 * scale);

        unsafe {
            Self {
                map,
                active_neurons: vec![],
                scale,
                max_distance,
                signal_strength,
                threshold,
                shuffler: None,
                refractory_period,
                random_activation: true,
                loaded_image: None,
                aggregated: Box::new_zeroed().assume_init(),
            }
        }
    }

    pub fn clear(&mut self) {
        self.aggregated.fill(0);
        self.active_neurons.clear();
        self.map.fill(Neuron::default());
    }

    pub fn rebuild_aggregated(&mut self) {
        let to_add = (255 / (self.scale * self.scale)) as u8;
        self.aggregated.fill(0);
        match &self.shuffler {
            None => self.active_neurons.iter().for_each(|(x, y)| {
                self.aggregated[(y / self.scale) * 1920 + x / self.scale] += to_add
            }),
            Some(shuffler) => unsafe {
                assert_eq!(shuffler.data().len(), self.map.data().len());
                self.active_neurons.iter().for_each(|coord| {
                    let (x, y) = shuffler.get_unchecked(*coord);
                    self.aggregated[(y / self.scale) * 1920 + x / self.scale] += to_add;
                });
            }
        }
    }

    pub fn activate_one(&mut self, x: usize, y: usize) {
        let state = &mut self.map[y][x].state;
        let to_add = (255 / (self.scale * self.scale)) as u8;
        match &state {
            Active => {}
            Inactive |
            Refractory { .. } => {
                *state = Active;
                self.active_neurons.push((x, y));
                self.aggregated[(y / self.scale) * 1920 + x / self.scale] += to_add;
            }
        }
    }
}

// This includes starting point
pub fn traverse<F>(
    start_x: usize,
    start_y: usize,
    max_distance: f32,
    boundary: (Range<usize>, Range<usize>),
    mut operation: F,
) where
    F: FnMut((usize, usize)),
{
    fn dist_sqr(x1: usize, y1: usize, x2: usize, y2: usize) -> usize {
        let d1 = x1.abs_diff(x2);
        let d2 = y1.abs_diff(y2);
        d1 * d1 + d2 * d2
    }

    let max_distance_sqr = max_distance * max_distance;
    let segment = max_distance.ceil() as usize;

    let bottom = start_y.saturating_sub(segment);
    let left = start_x.saturating_sub(segment);

    for y in bottom..=(start_y + segment) {
        for x in left..=(start_x + segment) {
            if !boundary.0.contains(&x) {
                continue;
            };
            if !boundary.1.contains(&y) {
                continue;
            };
            if dist_sqr(start_x, start_y, x, y) as f32 > max_distance_sqr {
                continue;
            }

            operation((x, y));
        }
    }
}
