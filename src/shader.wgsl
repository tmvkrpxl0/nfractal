@vertex
fn vs_main(@location(0) vertices: vec3<f32>) -> @builtin(position) vec4<f32> {
    return vec4<f32>(vertices, 1.0);
}

@group(0) @binding(0)
var world_texture: texture_2d<f32>;
@group(0) @binding(1)
var world_sampler: sampler;

@fragment
fn fs_main(@builtin(position) in: vec4<f32>) -> @location(0) vec4<f32> {
    let sample_x = in.x / 1920.0;
    let sample_y = in.y / 1080.0;
    let sample_pos = vec2<f32>(sample_x, sample_y);

    let sampled_result = textureSample(world_texture, world_sampler, sample_pos);
    let value = sampled_result.x;
    return vec4(value, value, value, 1.0);
}
